import Router from "express";

export default () => {
  const router = Router();

  router.get("/marketplace", async (req, res) => {
    res.json({ version: 1.0 });
  });

  router.get("/logistic-order-service", async (req, res) => {
    const logisticOrderService = req.scope.resolve("logisticOrderService");
    res.json(await logisticOrderService.getMessage());
  });

  return router;
};
