import { BeforeInsert, Column, Entity, Index } from "typeorm";
import { BaseEntity } from "@medusajs/medusa";
import { generateEntityId } from "@medusajs/medusa/dist/utils";

@Entity()
export class LogisticOrder extends BaseEntity {
  @Index("IDX_lo_marketplace_id")
  @Column({ type: "varchar" })
  marketplace_id: string;

  @Column({ type: "jsonb", nullable: true })
  metadata: Record<string, unknown>;

  @Index("IDX_lo_seller_id")
  @Column({ type: "int" })
  seller_id: number;

  @Column({ type: "varchar" })
  seller_name: string;

  @Column({ type: "varchar" })
  status: string;

  @BeforeInsert()
  private beforeInsert(): void {
    this.id = generateEntityId(this.id, "post");
  }
}
