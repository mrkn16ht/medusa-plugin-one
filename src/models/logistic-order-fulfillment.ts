import {
  BeforeInsert,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
} from "typeorm";
import { BaseEntity, Fulfillment } from "@medusajs/medusa";
import { generateEntityId } from "@medusajs/medusa/dist/utils";
import { LogisticOrder } from "./logistic-order";

@Entity()
export class LogisticOrderFulfillment extends BaseEntity {
  @ManyToOne(() => LogisticOrder)
  logistic_order_id: LogisticOrder;

  @Column()
  fulfillment_id: string;

  @OneToOne(() => Fulfillment)
  @JoinColumn({ name: "fulfillment_id" })
  fulfillment: Fulfillment;

  @BeforeInsert()
  private beforeInsert(): void {
    this.id = generateEntityId(this.id, "post");
  }
}
