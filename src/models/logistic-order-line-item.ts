import {
  BeforeInsert,
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToOne,
} from "typeorm";
import { BaseEntity, LineItem } from "@medusajs/medusa";
import { generateEntityId } from "@medusajs/medusa/dist/utils";
import { LogisticOrder } from "./logistic-order";

@Entity()
export class LogisticOrderLineItem extends BaseEntity {
  @Index("IDX_loli_logistic_order_id")
  @Column()
  logistic_order_id: string;

  @ManyToOne(() => LogisticOrder, { onDelete: "CASCADE" })
  @JoinColumn({ name: "logistic_order_id" })
  logistic_order: LogisticOrder;

  @Index("IDX_loli_line_item_id")
  @Column()
  line_item_id: string;

  @OneToOne(() => LineItem, { onDelete: "CASCADE" })
  @JoinColumn({ name: "line_item_id" })
  line_item: LineItem;

  @BeforeInsert()
  private beforeInsert(): void {
    this.id = generateEntityId(this.id, "post");
  }
}
