import { EntityRepository, Repository } from "typeorm";
import { LogisticOrderLineItem } from "../models/logistic-order-line-item";

@EntityRepository(LogisticOrderLineItem)
export class LogisticOrderLineItemRepository extends Repository<LogisticOrderLineItem> {}
