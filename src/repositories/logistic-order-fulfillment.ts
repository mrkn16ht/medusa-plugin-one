import { EntityRepository, Repository } from "typeorm";
import { LogisticOrderFulfillment } from "../models/logistic-order-fulfillment";

@EntityRepository(LogisticOrderFulfillment)
export class LogisticOrderFulfillmentRepository extends Repository<LogisticOrderFulfillment> {}
