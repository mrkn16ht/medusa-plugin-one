import { EntityRepository, Repository } from "typeorm";
import { LogisticOrder } from "../models/logistic-order";

@EntityRepository(LogisticOrder)
export class LogisticOrderRepository extends Repository<LogisticOrder> {}
