import { MigrationInterface, QueryRunner } from "typeorm";

export class logisticOrder1664352006996 implements MigrationInterface {
  name = "logisticOrder1664352006996";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "logistic_order" (
        "id" character varying NOT NULL,
        "marketplace_id" character varying, 
        "metadata" jsonb, 
        "seller_id" integer,
        "seller_name" character varying,
        "status" character varying,
        "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), 
        "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
        CONSTRAINT "PK_logistic_order" PRIMARY KEY ("id")
      );`
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_lo_marketplace_id" ON "logistic_order" ("marketplace_id");`
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_lo_seller_id" ON "logistic_order" ("seller_id");`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
