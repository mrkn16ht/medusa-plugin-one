import { MigrationInterface, QueryRunner } from "typeorm";

export class logisticOrderLineItem1664424215756 implements MigrationInterface {
  name = "logisticOrderLineItem1664424215756";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "logistic_order_line_item" (
        "id" character varying NOT NULL,
        "logistic_order_id" character varying,
        "line_item_id" character varying,
        "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), 
        "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
        CONSTRAINT "FK_loli_logistic_order_id" FOREIGN KEY ("logistic_order_id") REFERENCES "logistic_order"("id") ON DELETE CASCADE,
        CONSTRAINT "FK_loli_line_item_id" FOREIGN KEY ("line_item_id") REFERENCES "line_item"("id") ON DELETE CASCADE
      );`
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_loli_logistic_order_id" ON "logistic_order_line_item" ("logistic_order_id");`
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_loli_line_item_id" ON "logistic_order_line_item" ("line_item_id");`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
