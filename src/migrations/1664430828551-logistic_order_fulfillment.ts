import { MigrationInterface, QueryRunner } from "typeorm";

export class logisticOrderFulfillment1664430828551
  implements MigrationInterface
{
  name = "logisticOrderFulfillment1664430828551";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "logistic_order_fulfillment" (
        "id" character varying NOT NULL,
        "logistic_order_id" character varying,
        "fulfillment_id" character varying,
        "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), 
        "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
        CONSTRAINT "FK_lof_logistic_order_id" FOREIGN KEY ("logistic_order_id") REFERENCES "logistic_order"("id") ON DELETE CASCADE,
        CONSTRAINT "FK_lof_fulfillment_id" FOREIGN KEY ("fulfillment_id") REFERENCES "fulfillment"("id") ON DELETE CASCADE
      );`
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_lof_logistic_order_id" ON "logistic_order_fulfillment" ("logistic_order_id");`
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_lof_fulfillment_id" ON "logistic_order_fulfillment" ("fulfillment_id");`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
