import { TransactionBaseService } from "@medusajs/medusa";
import { BaseService } from "medusa-interfaces";

class LogisticOrderService extends TransactionBaseService {
  constructor({ logisticOrderRepository, manager }) {
    super({ logisticOrderRepository, manager });

    this.logisticOrderRepository = logisticOrderRepository;
    this.manager_ = manager;
  }
  getMessage() {
    console.log(`logistic order repository: ${this.logisticOrderRepository}`);
    const logisticOrderRepository = this.manager_.getCustomRepository(
      this.logisticOrderRepository
    );

    const test = logisticOrderRepository.count();
    console.log(test);
    console.log(`hey repo ${JSON.stringify(logisticOrderRepository)}`);
    return `Welcome to My Store!`;
  }
}
export default LogisticOrderService;
